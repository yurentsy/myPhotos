package com.yurentsy.myphotos.theme;

import com.yurentsy.myphotos.R;

public enum Theme {
    THEME_WHITE(R.style.AppTheme_WhiteTheme),
    THEME_BLACK(R.style.AppTheme_BlackTheme);

    private int value;

    Theme(int textViewStyle) {
        this.value = textViewStyle;
    }

    public int getValue() {
        return value;
    }

    public static int getNextTheme(int value){
        return (value == THEME_WHITE.value) ? THEME_BLACK.value : THEME_WHITE.value;
    }
}
