package com.yurentsy.myphotos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.yurentsy.myphotos.photos.Photo;

public class AlbumDetail extends AppCompatActivity {

    Toolbar toolbar;
    AppCompatTextView title;
    ImageView photo;

    String uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_detail);

        toolbar = findViewById(R.id.ad_toolbar);
        title = findViewById(R.id.ad_toolbar_title);
        photo = findViewById(R.id.ad_photo);

        setSupportActionBar(toolbar);
        uri = getIntent().getStringExtra("photo");
        Picasso.get().load(uri).into(photo);
    }

    public static Intent getIntent(Context context, Photo photo) {
        String uri = photo.getPhoto_75();
        if (photo.getPhoto_807() != null)
            uri = photo.getPhoto_807();
        else if (photo.getPhoto_604() != null)
            uri = photo.getPhoto_604();
        else if (photo.getPhoto_130() != null)
            uri = photo.getPhoto_130();

        Intent intent = new Intent(context, AlbumDetail.class);
        intent.putExtra("photo", uri);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_album_detail_context, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.ad_close:
                finish();
                break;
        }
        return true;
    }
}
