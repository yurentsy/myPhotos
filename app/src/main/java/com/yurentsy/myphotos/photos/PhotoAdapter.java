package com.yurentsy.myphotos.photos;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.yurentsy.myphotos.AlbumDetail;
import com.yurentsy.myphotos.MainActivity;
import com.yurentsy.myphotos.R;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

    private final MainActivity mainActivity;
    private List<Photo> photos = new LinkedList<>();

    public PhotoAdapter(MainActivity mainActivity, List<Photo> photos) {
        this.mainActivity = mainActivity;
        setItems(photos);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_photo, parent, false);
        PhotoAdapter.PhotoViewHolder pvh = new PhotoAdapter.PhotoViewHolder(view);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        Picasso.get().load(photos.get(position).getPhoto_130())
                .resizeDimen(R.dimen.fr_photo_height, R.dimen.fr_photo_height)
                .into(holder.photo);
        holder.photo.setOnClickListener(view -> {
//            Snackbar.make(mainActivity.rootLayout, "pressed on " + photos.get(position).getId(), Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
            mainActivity.startActivity(AlbumDetail.getIntent(mainActivity, photos.get(position)));
        });
    }

    @Override
    public int getItemCount() {
        return photos == null ? -1 : photos.size();
    }

    public void setItems(List<Photo> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fr_photo)
        ImageView photo;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }
    }
}
