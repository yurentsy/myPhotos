package com.yurentsy.myphotos.photos;

import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.yurentsy.myphotos.MainActivity;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Album {
    private AlbumObject album;
    private List<Photo> photos;
    private PhotoAdapter adapter;

    public Album(AlbumObject album) {
        this.album = album;
        getPhotos(album.getOwner_id(), album.getId());
    }

    public AlbumObject getAlbum() {
        return album;
    }

    public void setAlbum(AlbumObject album) {
        this.album = album;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public PhotoAdapter getAdapter(MainActivity parent) {
        if (adapter == null)
            adapter = new PhotoAdapter(parent, photos);
        return adapter;
    }

    private void getPhotos(String userId, String albumId) {
        new VKRequest("photos.get", VKParameters.from(VKApiConst.OWNER_ID, userId, VKApiConst.ALBUM_ID, albumId)).executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONArray jsonArray = response.json.getJSONObject("response").getJSONArray("items");
                    int length = jsonArray.length();
                    Photo photosArray[] = new Photo[length];
                    for (int i = 0; i < length; i++) {
                        photosArray[i] = new Photo(jsonArray.getJSONObject(i));
                    }
                    photos = new LinkedList<>(Arrays.asList(photosArray));
                    adapter.setItems(photos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
