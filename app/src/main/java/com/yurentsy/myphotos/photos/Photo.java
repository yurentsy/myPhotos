package com.yurentsy.myphotos.photos;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

public class Photo implements Serializable {

    private String id;
    private String album_id;
    private String owner_id;
    private String photo_75;
    private String photo_130;
    private String photo_604;
    private String photo_807;
    private String width;
    private String height;
    private String text;
    private String date;

    public String getId() {
        return id;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public String getPhoto_75() {
        return photo_75;
    }

    public String getPhoto_130() {
        return photo_130;
    }

    public String getPhoto_604() {
        return photo_604;
    }

    public String getPhoto_807() {
        return photo_807;
    }

    public String getWidth() {
        return width == null ? "400" : width;
    }

    public String getHeight() {
        return height == null ? "400" : height;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    public Photo(JSONObject jsonObject) {
        Thread th = new Thread(() -> {
            if (jsonObject == null) return;
            setFromJson(jsonObject);
        });
        th.start();
        try {
            th.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setFromJson(JSONObject json) {
        final Photo photo = new Gson().fromJson(json.toString(), Photo.class);

        initPhoto(photo);
    }

    private void initPhoto(Photo photo) {
        id = photo.getId();
        album_id = photo.getAlbum_id();
        owner_id = photo.getOwner_id();
        photo_75 = photo.getPhoto_75();
        photo_130 = photo.getPhoto_130();
        photo_604 = photo.getPhoto_604();
        photo_807 = photo.getPhoto_807();
        width = photo.getWidth();
        height = photo.getHeight();
        text = photo.getText();
        date = photo.getDate();
    }
}
