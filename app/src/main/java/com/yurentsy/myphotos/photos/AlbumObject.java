package com.yurentsy.myphotos.photos;

import android.os.Parcelable;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

public class AlbumObject implements Serializable {
    private String id;
    private String thumb_id;
    private String owner_id;
    private String title;
    private String description;
    private String created;
    private String updated;
    private String size;
    private String thumb_is_last;
    private Privacy privacy_view;
    private Privacy privacy_comment;

    private class Privacy {
        String Type;
    }

    public String getId() {
        return id;
    }

    public String getThumb_id() {
        return thumb_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCreated() {
        return created;
    }

    public String getUpdated() {
        return updated;
    }

    public String getSize() {
        return size;
    }

    public String getThumb_is_last() {
        return thumb_is_last;
    }

    public Privacy getPrivacy_view() {
        return privacy_view;
    }

    public Privacy getPrivacy_comment() {
        return privacy_comment;
    }

    public AlbumObject(JSONObject jsonObject) {
        Thread th = new Thread(() -> {
            if (jsonObject == null) return;
            setFromJson(jsonObject);
        });
        th.start();
        try {
            th.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setFromJson(JSONObject json) {
        final AlbumObject albumObject = new Gson().fromJson(json.toString(), AlbumObject.class);

        initAlbum(albumObject);
    }

    private void initAlbum(AlbumObject albumObject) {
        id = albumObject.getId();
        thumb_id = albumObject.getThumb_id();
        owner_id = albumObject.getOwner_id();
        title = albumObject.getTitle();
        description = albumObject.getDescription();
        created = albumObject.getCreated();
        updated = albumObject.getUpdated();
        size = albumObject.getSize();
        thumb_is_last = albumObject.getThumb_is_last();
        privacy_view = albumObject.getPrivacy_view();
        privacy_comment = albumObject.getPrivacy_comment();
    }
}

//public class AlbumObject {
//    String title;
//    String count;
//    List<Token> photos = new LinkedList<>();
//    private PhotoAdapter adapter;
//
//    public static final List<AlbumObject> ALBUMS = Arrays.asList(
//            newRandomAlbum(1), newRandomAlbum(2), newRandomAlbum(3), newRandomAlbum(4), newRandomAlbum(5), newRandomAlbum(6)
//    );
//
//    public AlbumObject(String title) {
//        this.title = title;
//    }
//
//    public static AlbumObject newRandomAlbum(Integer index) {
//        AlbumObject album = new AlbumObject("album_" + index);
//        int size = new Random().nextInt(8) + 2;
//        album.count = String.valueOf(size) + " photos";
//        while (size-- > 0) {
//            album.photos.add(Token.getRandomToken());
//        }
//        return album;
//    }
//
//    public PhotoAdapter getAdapter(MainActivity mainActivity) {
//        if (adapter == null)
//            adapter = new PhotoAdapter(mainActivity, photos);
//        return adapter;
//    }
//}
