package com.yurentsy.myphotos.photos;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yurentsy.myphotos.AlbumDetail;
import com.yurentsy.myphotos.MainActivity;
import com.yurentsy.myphotos.R;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

    private MainActivity mainActivity;
    List<Album> albums = new LinkedList<>();

    public AlbumAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
//        albums.addAll(AlbumObject.ALBUMS);
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_album, parent, false);
        AlbumViewHolder avh = new AlbumViewHolder(view);
        return avh;
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, int position) {
        holder.title.setText(albums.get(position).getAlbum().getTitle());
        holder.count.setText(albums.get(position).getAlbum().getSize() + " photos");

        holder.photos.setLayoutManager(new LinearLayoutManager(mainActivity.getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        holder.photos.setAdapter(albums.get(position).getAdapter(mainActivity));

        holder.setOnClickListener(view -> {
//            int itemAdapterPosition = mainActivity.recyclerView.getChildAdapterPosition(view);

//            Snackbar.make(mainActivity.rootLayout, "pressed on " + albums.get(position).getAlbum().getTitle(), Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
        });
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public void setItems(List<Album> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.af_title)
        TextView title;
        @BindView(R.id.af_count)
        TextView count;
        @BindView(R.id.af_photos)
        RecyclerView photos;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }

    }
}
