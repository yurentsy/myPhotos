package com.yurentsy.myphotos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;
import com.yurentsy.myphotos.photos.Album;
import com.yurentsy.myphotos.photos.AlbumAdapter;
import com.yurentsy.myphotos.photos.AlbumObject;
import com.yurentsy.myphotos.theme.Theme;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG_VK = "VK";

    private String[] scope = new String[]{VKScope.PHOTOS};
    public AlbumAdapter adapter;
    private int theme;
    private SharedPreferences sharedPreferences;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.albums) RecyclerView recyclerView;
    @BindView(R.id.toolbar_layout) public CoordinatorLayout rootLayout;

    private ImageView photoUser;
    private TextView firstName;
    private TextView lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        applyTheme();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
//        Log.i(TAG_VK, java.util.Arrays.asList(fingerprints).toString());

        init();
        if (VKSdk.isLoggedIn()) {
            getMe();
        } else {
            VKSdk.login(this, scope);
        }
    }

    @Override
    protected void onDestroy() {
        sharedPreferences.edit().putInt("Theme", theme).apply();
        super.onDestroy();
    }

    private void applyTheme() {
        theme = getThemeFromPreference();
        setTheme(theme);
    }

    public int getThemeFromPreference() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                return bundle.getInt("Theme", R.style.AppTheme_WhiteTheme);
            }
        }
        return sharedPreferences != null ? sharedPreferences.getInt("Theme", R.style.AppTheme_WhiteTheme) : R.style.AppTheme_WhiteTheme;
    }

    private void getMe() {
        VKApi.users().get().executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                VKApiUser user = (VKApiUser) ((VKList) response.parsedModel).get(0);
                getUsers();
                getAlbums();
            }
        });
    }

    private void getUsers() {
        new VKRequest("users.get", VKParameters.from(VKApiConst.USER_ID, getMyId(), VKApiConst.FIELDS, "photo_50,photo_100, photo_200_orig, photo_200, photo_400_orig")).executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONObject jsonObject = response.json.getJSONArray("response").getJSONObject(0);
                    Picasso.get().load(jsonObject.getString("photo_200")).into(photoUser);
                    firstName.setText(jsonObject.getString("first_name"));
                    lastName.setText(jsonObject.getString("last_name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getAlbums() {
        new VKRequest("photos.getAlbums", VKParameters.from(VKApiConst.OWNER_ID, getMyId())).executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONArray jsonArray = response.json.getJSONObject("response").getJSONArray("items");
                    int length = jsonArray.length();
                    final Album[] albums = new Album[length];
                    for (int i = 0; i < length; i++) {
                        AlbumObject albumObject = new AlbumObject(jsonArray.getJSONObject(i));
                        albums[i] = new Album(albumObject);

//                        getPhotos(userId, albumObject.getId());
                    }
                    adapter.setItems(Arrays.asList(albums));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    String getMyId() {
        final VKAccessToken vkAccessToken = VKAccessToken.currentToken();
        return vkAccessToken != null ? vkAccessToken.userId : "0";
    }

    private void init() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        photoUser = navigationView.getHeaderView(0).findViewById(R.id.nh_photo_user);
        firstName = navigationView.getHeaderView(0).findViewById(R.id.nh_first_name);
        lastName = navigationView.getHeaderView(0).findViewById(R.id.nh_last_name);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);

        adapter = new AlbumAdapter(this);
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                getMe();
            }

            @Override
            public void onError(VKError error) {
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
//            case R.id.nav_gallery:
//                Toast.makeText(this, "nav_gallery", Toast.LENGTH_SHORT).show();
//                break;
            case R.id.nav_manage:
                switchTheme();
                break;
            case R.id.nav_send:
                VKSdk.logout();
                logout();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void switchTheme() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Theme", Theme.getNextTheme(theme));
        startActivity(intent);
        finish();
    }
}
